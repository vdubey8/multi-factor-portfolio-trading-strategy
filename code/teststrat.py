import datetime
import numpy as np
np.seterr(divide='ignore')
import pandas as pd
import time
import urllib2
import random
import csv
import statsmodels.formula.api as sm
import seaborn as sns
import warnings
warnings.filterwarnings("ignore")


class SecurityData(object):
    def __init__(self, fl_name, n, m, L, sd, ed, scoring=1, verbose=False):
        self.ticker_file = fl_name
        self.ed = ed
        self.sd = sd
        self.n = n
        self.m = m
        self.L = L
        # scoring: 1 = Dynamic weights, 0 = Fixed Weights
        self.scoring = scoring

    def read_file(self, headerRow=0, encodingType='gb18030'):
        data = pd.read_csv(self.ticker_file, usecols=(0, 1), header=headerRow, encoding=encodingType)
        return data

    def get_historic_data(self, ticker, mktshare):

        build_url = "http://ichart.finance.yahoo.com/table.csv?s=%s&a=%s&b=%s&c=%s&d=%s&e=%s&f=%s" % \
            (ticker, self.sd[1] - 1, self.sd[2], self.sd[0], self.ed[1] - 1, self.ed[2], self.ed[0])

        try:
            yahoo_data = urllib2.urlopen(build_url).readlines()

            date_list = []
            hist_data = [[] for i in range(6)]

            # Skip Header
            for day in yahoo_data[1:]:
                headers = day.rstrip().split(',')
                date_list.append(datetime.datetime.strptime(headers[0], '%Y-%m-%d'))
                for i, header in enumerate(headers[1:]):
                    hist_data[i].append(float(header))

            # Create and Sort DataFrame
            hist_data = dict(zip(['open', 'high', 'low', 'close', 'volume', 'adj_close'], hist_data))
            df_main = pd.DataFrame(hist_data, index=pd.Index(date_list)).sort_index()
            
            # Get Factors
            df_main['Market Share'] = mktshare
            df_main['Market Cap'] = df_main['Market Share'] * df_main['adj_close']
            df_main['Stock'] = ticker
            returns = (df_main['adj_close']-df_main['adj_close'].shift(1))/df_main['adj_close'].shift(1)
            log_ret = np.log((df_main['adj_close']))-np.log((df_main['adj_close'].shift(1)))
            Momentum = np.log(df_main['adj_close'].shift(1)) - np.log(df_main['adj_close'].shift(self.n))
            reversal = np.log(df_main['adj_close'].shift(self.m)) - np.log(df_main['adj_close'].shift(1))
            Volatility = np.log(pd.rolling_std(log_ret.shift(1), self.L))
            df_main['Returns'] = returns
            df_main['Momentum'] = Momentum
            df_main['Volatility'] = Volatility
            df_main['Reversal'] = reversal

            try:
                headerRow = 0
                accountingRatios = pd.read_csv('./data/sz/%s_factor.csv' % ticker, header=headerRow, encoding='gb18030')
                accountingRatios = accountingRatios.set_index('Date')
                df_main = df_main.merge(accountingRatios, left_index=True, right_index=True)

            except Exception, e:
                try:
                    headerRow=0
                    accountingRatios = pd.read_csv('./data/ss/%s_factor.csv' % ticker, header=headerRow, encoding='gb18030')
                    accountingRatios = accountingRatios.set_index('Date')
                    df_main = df_main.merge(accountingRatios, left_index=True, right_index=True)

                except Exception, e:
                    print "Reading Stock CSV data."
                    pass

            # Determine MScore Weights using Regression
            test = df_main[['Returns', 'PB', 'PCF', 'PE', 'PS', 'Momentum', 'Reversal', 'Volatility']]
            test = test.dropna(axis=0)
            test_coefs = self.Regression(test)
            sum_coefs = test_coefs.sum(axis=1)
            test_coefs[0] /= sum_coefs 
            test_coefs[1] /= sum_coefs
            test_coefs[2] /= sum_coefs
            test_coefs[3] /= sum_coefs
            test_coefs[4] /= sum_coefs
            test_coefs[5] /= sum_coefs
            test_coefs[6] /= sum_coefs

            if self.scoring:
                MScore = test_coefs[0].values[0] * df_main['PB'] + test_coefs[1].values[0] * df_main['PCF'] + test_coefs[2].values[0] * df_main['PE'] + test_coefs[3].values[0] * df_main['PS'] + test_coefs[4].values[0] * df_main['Momentum'] + test_coefs[5].values[0] * df_main['Reversal'] + test_coefs[6].values[0] * df_main['Volatility']
            else:
                MScore = 0.1 * df_main['PB'] + 0.05 * df_main['PCF'] + 0.1 * df_main['PE'] + 0.1 * df_main['PS'] + 0.3 * df_main['Momentum'] + 0.3 * df_main['Reversal'] + 0.35 * df_main['Volatility']

            df_main['MScore'] = MScore

            return df_main

        except Exception, e:
                print "Reading Yahoo CSV Data"
                pass
            
    def Regression(self, test):
        clf = sm.OLS(test[['PB', 'PCF', 'PE', 'PS', 'Momentum', 'Reversal', 'Volatility']], test['Returns']).fit()
        return clf.params


class Strategy(object):
    def __init__(self, verbose=False):
        pass

    # Select Stocks from the Universe with Top 100 MScore
    def select_stocks(self, data, i, value):

        # Create a DataFrame with columns: Ticker, MScore, Close, Shares
        df_mscore = pd.DataFrame(columns=['Ticker', 'MScore', 'Close', 'Shares'])
        df_mscore['Ticker'] = data.keys()

        # Iterate over the DataDic
        j = 0
        for ticker in data.keys():
            # Check valid MScore, Close and Shares values
            try:
                df_mscore['MScore'][j] = data[ticker]['MScore'][i]
                df_mscore['Close'][j] = data[ticker]['close'][i]
                df_mscore['Shares'][j] = value/data[ticker]['close'][i]
            except:
                df_mscore['MScore'][j] = 0
                df_mscore['Close'][j] = 0
                df_mscore['Shares'][j] = 0
            j += 1

        df_mscore = df_mscore.sort('MScore', ascending=False)
        mscore_top = df_mscore.head(100)
        return mscore_top

    def sell_signals(self, init_port, stocks_tosell, data, i, freq):

        gain = 0
        k = 0

        init_port = init_port.reset_index(drop=True)

        for ticker in init_port['Ticker']:
            if ticker in stocks_tosell:
                try:
                    currentPrice = data[ticker]['close'][i]
                except:
                    currentPrice = data[ticker]['close'][i-freq]
                gain = gain + (currentPrice * init_port['Shares'][k])

                init_port = init_port[init_port['Ticker'] != ticker]
            k += 1

        return gain, init_port

    def buy_signals(self, init_port, stocks_tobuy, data, i, portfolioFactor):

        loss = 0

        init_port = init_port.reset_index(drop=True)
        for ticker in stocks_tobuy:

            # Calculate all the Current Values
            currentPrice = data[ticker]['close'][i]
            shares = portfolioFactor/currentPrice
            loss += portfolioFactor
            temp = [ticker, currentPrice, shares]

            init_port.loc[len(init_port)] = temp

        return loss, init_port


class Portfolio(object):
    def __init__(self, sampling='insample', verbose=False):
        self.sampling = sampling
        self.returns = []

    def calc_portfolio_val(self, previousPortfolioList, data, i, freq):
        sumFactor = 0
        k = 0
    
        # Iterate over the Ticker
        for ticker in previousPortfolioList['Ticker']:
            try:
                sumFactor = sumFactor + (data[ticker]['close'][i] * previousPortfolioList['Shares'][k])
            except:
                sumFactor = sumFactor + (data[ticker]['close'][i - freq] * previousPortfolioList['Shares'][k])
            k += 1
    
        return sumFactor

    def rebalance_portfolio(self, init_port, data, i, portfolioFactor):
        
        init_port = init_port.reset_index(drop=True)
        j = 0
        loss_rebalance = 0
        gain_rebalance = 0
        
        for ticker in init_port['Ticker']:
            currentPrice = init_port['Close'][j]
            oldShares = init_port['Shares'][j]
           
            new_shares = portfolioFactor/currentPrice
            diff = new_shares - oldShares
           
            if diff > 0:
                loss_rebalance = loss_rebalance + diff * currentPrice
                temp = [ticker, currentPrice, new_shares]
                init_port.loc[j] = temp
    
            if diff < 0:
                gain_rebalance = gain_rebalance + abs(diff)*currentPrice
                temp = [ticker, currentPrice, new_shares]
                init_port.loc[j] = temp
               
            else:
                pass
            j += 1
       
        return loss_rebalance, gain_rebalance, init_port

    def save_output(self, current_portfolio, transaction, profit, i, freq):
        csv_data = []
        header = ["T0-TN", "Portfolio Value", "Transaction Value", "Sharpe Ratio", "PnL", "Daily Return", "MAVG(Returns)"]
        csv_data.append(header)
        j = 0
        
        periodicreturns = []
        returns = 0
        movingavg = 0
        sharpe = 0
        md = 0
        highestPortVal = current_portfolio[0]
        
        for m in current_portfolio:
            if j != 0:
                returns = (m-current_portfolio[j-1])/current_portfolio[j-1]
                periodicreturns.append(returns)
                standard = np.std(periodicreturns)
                movingavg = np.mean(periodicreturns)
                sharpe = (np.mean(periodicreturns)-0.03)/standard
            else:
                periodicreturns.append(returns)
                pass
    
            if m > highestPortVal:
                highestPortVal = m
                
            dd = (highestPortVal-m)/highestPortVal
            if dd > md:
                md = dd
            # Put it in the CSV Row
            row = [i, current_portfolio[j], transaction[j], sharpe, profit[j], returns, movingavg]
            self.returns.append(returns)
            # Iterate
            i += freq
            
            # Append the Row to CSV
            csv_data.append(row)
            
            j += 1

        # Calculate Performance Metrics
        cum_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = self.get_portfolio_stats(current_portfolio)
        if self.sampling == 'insample':        
            print '---------------Backtest Results----------------'
        else:
            print '-------------Out of Sample Results-------------'
        print 'Cumulative Return: ', cum_ret
        print 'Average Daily Return: ', avg_daily_ret
        print 'Standard Deviation of Returns: ', std_daily_ret
        print 'Sharpe Ratio: ', sharpe_ratio
        print '------------------------------------------------'
        self.histo_returns()
        
        # Append Performance Metrics
        csv_data.append(["Total PnL:", sum(profit)])
        csv_data.append(["Cumulative Return:", cum_ret])
        csv_data.append(["Avg Daily Return:", avg_daily_ret])
        csv_data.append(["Std Dev of Returns:", std_daily_ret])
        csv_data.append(["Sharpe Ratio:", sharpe_ratio])
        csv_data.append(["Max Drawdown:", md])

        return csv_data

    def get_portfolio_stats(self, port_val, rfr=0.0, sf=252.0):

        port_val = pd.Series(port_val)
        dr = port_val.pct_change(1)  # Daily returns
        adr = dr.mean()  # Average Daily returns
        cumr = port_val / port_val[0] - 1  # Cumulative returns vector
        cr = cumr.iloc[-1]  # Cumulative returns of the whole duration
        sddr = dr.std()  # Standard deviation of returns (Volatility)
        sr = (adr / sddr) * np.sqrt(sf)  # Sharpe Ratio
        return cr, adr, sddr, sr
    
    def histo_returns(self):
        
        # plt.hist(self.returns, alpha=0.6, color='g')
        # plt.title('Distribution of Returns')
        # plt.savefig('returns_'+self.sampling+'.png')
        sns.set_style('whitegrid')
        sns_plot = sns.kdeplot(np.array(self.returns), bw=0.5)
        sns_plot.figure.savefig('returns_'+self.sampling+'.png')

        
if __name__ == '__main__':
    start_time = time.time()
    ticker_file = './ticker_universe.csv'
    encodingType = 'gb18030'
    headerRow = 0
    # Initial Portfolio Value
    portfolioValue = 10000000

    # Initialize classes
    sec = SecurityData(fl_name=ticker_file, n=5, m=20, L=15, sd=(2011, 1, 4), ed=(2014, 11, 31), scoring=1, verbose=False)
    strat = Strategy()
    port = Portfolio()

    # Retrieve the Market List and Ticker List.
    mkt_list = sec.read_file(headerRow, encodingType)

    # Iterate over the Market List DataFrame
    for i in range(len(mkt_list)):
        temp = mkt_list["ticker"][i]

        if "SH" in temp:
            mkt_list["ticker"][i] = mkt_list["ticker"][i].replace("SH", "SS")
        else:
            pass

    for i in range(len(mkt_list)):
        mkt_list["mktshare"][i] = float(mkt_list["mktshare"][i].replace(",", ""))

    # Number of Stocks in the Universe
    subsetData = random.sample(range(2784), 1000)
    data = {}
    days = []

    for i in subsetData:

        try:
             ticker_data = sec.get_historic_data(mkt_list['ticker'][i], mkt_list['mktshare'][i])
             days.append(len(ticker_data))
             data[mkt_list['ticker'][i]] = ticker_data
             #time.sleep(2)
        except:
             pass

    # Iteration to skip NaN values
    i = 20
    firstiteratei = i

    # Rebalancing Frequency
    freq = 20

    # Number of Samples to be Chosen
    k = 100

    top_stocks = strat.select_stocks(data, i, (portfolioValue/k))

    # Select Top 100 Stocks with highest MScore
    init_port = top_stocks
    init_port = init_port.drop('MScore', 1)

    portfolioValuelist = []
    portfolioValuelist.append(portfolioValue)

    # Maintain PnL List
    pnl_list = []
    pnl_list.append(-portfolioValue-portfolioValue*0.001)

    # Transaction Cost List
    transaction_val = []
    transaction_val.append(portfolioValue*0.001)

    while i < 1001:

        old_portvals = portfolioValue

        # Rebalance Every 20 Days
        i += 20

        # Drop Index and Re-initiate the List with the Previous List.
        previousPortfolioList = init_port
        previousPortfolioList = previousPortfolioList.reset_index(drop=True)

        # Calculate the new Portfolio Values
        sumFactor = port.calc_portfolio_val(previousPortfolioList, data, i, freq)
        portfolioValue = sumFactor

        top_stocks = strat.select_stocks(data, i, (portfolioValue/k))

        # Start Rebalancing

        # Get Selling Signals
        stocks_tosell = list(set(previousPortfolioList['Ticker']) - set(top_stocks['Ticker']))
        gain, init_port = strat.sell_signals(init_port, stocks_tosell, data, i, freq)

        # Get Buying Signals
        stocks_tobuy = list(set(top_stocks['Ticker']) - set(previousPortfolioList['Ticker']))
        loss, init_port = strat.buy_signals(init_port, stocks_tobuy, data, i, (portfolioValue/k))

        # Rebalance the Holdings
        loss_rebalance, gain_rebalance, init_port = port.rebalance_portfolio(init_port, data, i, (portfolioValue/k))

        # New Portfolio Value
        portfolioValuelist.append(portfolioValue)

        # Calculate the Total Transaction Cost
        transactioncost = (abs(gain)+abs(loss)+abs(loss_rebalance)+abs(gain_rebalance))*0.001
        transaction_val.append(transactioncost)

        # Update profits
        profit = portfolioValue-old_portvals-transactioncost
        pnl_list.append(profit)

    # Save BackTesting Metrics
    dataSet = port.save_output(portfolioValuelist, transaction_val, pnl_list, firstiteratei, freq)
    with open('results.csv', 'wb') as filecsv:
        write_data = csv.writer(filecsv, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

        for row in dataSet:
            write_data.writerow(row)

    filecsv.close()

    end_time = time.time()
    print("Elapsed time was %g seconds" % (end_time - start_time))
