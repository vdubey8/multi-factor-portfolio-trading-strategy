Directory Tree:

-main folder
	-data/
		-ss/
		-sz/
	Readme.txt
	Report.docx
	results.csv
	results_out.csv
	returns_insample.png
	returns_outsample.png
	teststrat.py
	teststrat_outsample.py
	ticker_universe.csv

--------------------------------------------------------

#######Instructions:

To run insample code: python teststrat.py
To run outofsample code: python teststrat_outsample.py

All the csv files must be inside /data/ folder under /ss/ or /sz/ folders.

--------------------------------------------------------

#######Modules used:

Python		(2.7.11)
Anaconda	(4.0.0)
Pandas 		(0.17.1)
NumPy 		(1.10.1)
SciPy 		(0.17.0)
Seaborn 	(0.7.0)
Statsmodels	(0.6.1)
matplotlib	(1.5.1)

--------------------------------------------------------

######File: teststrat.py

Output: results.csv, results_insample.png

Description: Runs the Multi-Factor Trading Strategy on the in-sample data and returns results.

Classes: SecurityData, Strategy, Portfolio

//////Functions:
*SecurityData()
	-read_file()
	Reads the ticker CSV and returns the dataframe.

	-get_historic_data()
	Reads ticker data from yahoo and returns the dataframe.

	-Regression()
	Performs OLS regression on the factors selected against daily returns to generate weights for MScore calc.	

*Strategy()
	-select_stocks()
	Selects top 100 stocks from the universe based on MScore.
	
	-sell_signals()
	Generates sell signals for the portfolio.

	-buy_signals()
	Generates Buy signals for the portfolio.	

*Portfolio()
	-calc_portfolio_val()
	Returns daily portfolio value based on the price and stock holdings.

	-rebalance_portfolio()
	Rebalances portfolio based on the given frequency.	

	-save_output()
	Generates output for the data and returns array to store in CSV.

	-get_portfolio_stats()
	Generates performance metrics for the portfolio.

	-histo_returns()
	Generates daily return distribution.




######File: teststrat_outsample.py

Output: results_out.csv, results_outsample.png

Description: Runs the Multi-Factor Trading Strategy on the out of sample data and returns results.






