from teststrat import *
import random
import time
import csv
import warnings
warnings.filterwarnings("ignore")

if __name__ == "__main__":
    start_time = time.time()
    ticker_file = './ticker_universe.csv'
    encodingType = 'gb18030'
    headerRow = 0
    # Initial Portfolio Value
    portfolioValue = 10000000

    # Initialize classes
    sc = SecurityData(fl_name=ticker_file, n=5, m=20, L=15, sd=(2014, 11, 1), ed=(2015, 7, 31), scoring=1, verbose=False)
    strat = Strategy()
    port = Portfolio(sampling='outsample')

    # Retrieve the Market List and Ticker List.
    mkt_list = sc.read_file(headerRow, encodingType)

    # Iterate over the Market List DataFrame
    for i in range(len(mkt_list)):
        temp = mkt_list["ticker"][i]

        if "SH" in temp:
            mkt_list["ticker"][i] = mkt_list["ticker"][i].replace("SH", "SS")
        else:
            pass

    for i in range(len(mkt_list)):
        mkt_list["mktshare"][i] = float(mkt_list["mktshare"][i].replace(",", ""))

    # Number of Stocks in the Universe
    subsetData = random.sample(range(2784), 1000)
    data = {}
    days = []

    for i in subsetData:

        try:
             ticker_data = sc.get_historic_data(mkt_list['ticker'][i], mkt_list['mktshare'][i])
             days.append(len(ticker_data))             
             data[mkt_list['ticker'][i]] = ticker_data
             #time.sleep(2)
        except:
             pass

    # Iteration to skip NaN values
    i = 20
    firstiteratei = i

    # Rebalancing Frequency
    freq = 20

    # Number of Samples to be Chosen
    k = 100

    top_stocks = strat.select_stocks(data, i, (portfolioValue/k))

    # Select Top 100 Stocks with highest MScore
    init_port = top_stocks
    init_port = init_port.drop('MScore', 1)

    portfolioValuelist = []
    portfolioValuelist.append(portfolioValue)

    # Maintain PnL List
    pnl_list = []
    pnl_list.append(-portfolioValue-portfolioValue*0.001)

    # Transaction Cost List
    transaction_val = []
    transaction_val.append(portfolioValue*0.001)

    while i < 177:

        oldportfolioValue = portfolioValue

        # Rebalance Every 20 Days
        i += 20

        # Drop Index and Re-initiate the List with the Previous List.
        previousPortfolioList = init_port
        previousPortfolioList = previousPortfolioList.reset_index(drop=True)

        # Calculate the new Portfolio Values
        sumFactor = port.calc_portfolio_val(previousPortfolioList, data, i, freq)
        portfolioValue = sumFactor

        top_stocks = strat.select_stocks(data, i, (portfolioValue/k))

        # Start Rebalancing

        # Get Selling Signals
        stocks_tosell = list(set(previousPortfolioList['Ticker']) - set(top_stocks['Ticker']))
        gain, init_port = strat.sell_signals(init_port, stocks_tosell, data, i, freq)

        # Get Buying Signals
        stocks_tobuy = list(set(top_stocks['Ticker']) - set(previousPortfolioList['Ticker']))
        loss, init_port = strat.buy_signals(init_port, stocks_tobuy, data, i, (portfolioValue/k))

        # Rebalance Holdings
        loss_rebalance, gain_rebalance, init_port = port.rebalance_portfolio(init_port, data, i, (portfolioValue/k))

        # New Portfolio Value
        portfolioValuelist.append(portfolioValue)

        # Calculate the Total Transaction Cost
        transactioncost = (abs(gain)+abs(loss)+abs(loss_rebalance)+abs(gain_rebalance))*0.001
        transaction_val.append(transactioncost)

        # Update the Profits
        profit = portfolioValue-oldportfolioValue-transactioncost
        pnl_list.append(profit)

    # BackTesting Metrics
    dataSet = port.save_output(portfolioValuelist, transaction_val, pnl_list, firstiteratei, freq)
    with open('results_out.csv', 'wb') as filecsv:
        write_data = csv.writer(filecsv, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

        for row in dataSet:
            write_data.writerow(row)

    filecsv.close()
    
    end_time = time.time()
    print("Elapsed time was %g seconds" % (end_time - start_time))